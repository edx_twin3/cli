/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "FreeRTOS_CLI.h"
#include "FreeRTOS_DriverInterface.h"
#include "task.h"
#include "edx_xprintf.h"
#include "commands.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define cmdMAX_INPUT_SIZE			(50)
/* Block times of 50 and 500milliseconds, specified in ticks. */
#define cmd50ms						( ( void * ) ( 50UL / portTICK_RATE_MS ) )
#define cmd500ms					( ( void * ) ( 500UL / portTICK_RATE_MS ) )
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
#ifndef TEST
static TaskHandle_t xCMDS_UART = NULL;
uint8_t UART_rxBuffer;
static const char *welcome = ">";
static const int8_t * const pcEndOfCommandOutputString = ( int8_t * ) "\r\n[Press ENTER to execute the previous command again]\r\n>";
static const int8_t * const pcNewLine = ( int8_t * ) "\r\n";
#endif
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
#ifdef TEST
static void vLed(void *pvParameters){
	TickType_t xLastWakeTime;
	xLastWakeTime = xTaskGetTickCount();
	while(1){
		 vTaskDelayUntil( &xLastWakeTime, pdMS_TO_TICKS(250));
		HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);
		xprintf("%s\n", (uint8_t *)pvParameters);
	}
}
#else
static void xCLI(void *pvParameters){
	unsigned char cInputIndex = 0;
	char *pcOutputString;
	static char cInputString[ cmdMAX_INPUT_SIZE ], cLastInputString[ cmdMAX_INPUT_SIZE ];
	portBASE_TYPE res;
	Peripheral_Descriptor_t xConsole;
	pcOutputString = FreeRTOS_CLIGetOutputBuffer();
	xConsole = FreeRTOS_open( boardCOMMAND_CONSOLE_UART, ( uint32_t ) 0 );
	configASSERT( xConsole );
	res = FreeRTOS_ioctl( xConsole, ioctlUSE_ZERO_COPY_TX, 0 );
	configASSERT(res);
#if !configUSE_TASK_NOTIFICATIONS
	res = FreeRTOS_ioctl( xConsole, ioctlUSE_CHARACTER_QUEUE_RX, ( void * ) cmdMAX_INPUT_SIZE );
	configASSERT(res);
#endif
	res = FreeRTOS_ioctl( xConsole, ioctlSET_INTERRUPT_PRIORITY, ( void * ) ( configLIBRARY_LOWEST_INTERRUPT_PRIORITY - 1 ) );
	configASSERT(res);
	if( FreeRTOS_ioctl( xConsole, ioctlOBTAIN_WRITE_MUTEX, cmd50ms ) == pdPASS ){
	FreeRTOS_write( xConsole, welcome, strlen( welcome) ); }
	while(1){
#if configUSE_TASK_NOTIFICATIONS
	//According to me this must work :v
	ulTaskNotifyTake(0, portMAX_DELAY);
#else
		/* Only interested in reading one character at a time. */
		FreeRTOS_read( xConsole, &UART_rxBuffer, sizeof(UART_rxBuffer) );
#endif
		if( UART_rxBuffer == '\n' )
				{
					/* The input command string is complete.  Ensure the previous
					UART transmission has finished before sending any more data.
					This task will be held in the Blocked state while the Tx completes,
					if it has not already done so, so no CPU time will be wasted by
					polling. */
					if( FreeRTOS_ioctl( xConsole, ioctlOBTAIN_WRITE_MUTEX, cmd50ms ) == pdPASS )
					{
						/* Start to transmit a line separator, just to make the output
						easier to read. */
						FreeRTOS_write( xConsole, pcNewLine, strlen( ( char * ) pcNewLine ) );
					}

					/* See if the command is empty, indicating that the last command is
					to be executed again. */
					if( cInputIndex == 0 )
					{
						strcpy( ( char * ) cInputString, ( char * ) cLastInputString );
					}

					/* Pass the received command to the command interpreter.  The
					command interpreter is called repeatedly until it returns
					pdFALSE as it might generate more than one string. */
					do
					{
						/* Once again, just check to ensure the UART has completed
						sending whatever it was sending last.  This task will be held
						in the Blocked state while the Tx completes, if it has not
						already done so, so no CPU time	is wasted polling. */
						res = FreeRTOS_ioctl( xConsole, ioctlOBTAIN_WRITE_MUTEX, cmd50ms );
						if( res == pdPASS )
						{
							/* Get the string to write to the UART from the command
							interpreter. */
							res = FreeRTOS_CLIProcessCommand( cInputString, pcOutputString, configCOMMAND_INT_MAX_OUTPUT_SIZE );

							/* Write the generated string to the UART. */
							FreeRTOS_write( xConsole, pcOutputString, strlen( ( char * ) pcOutputString ) );
						}

					} while( res != pdFALSE );

					/* All the strings generated by the input command have been sent.
					Clear the input	string ready to receive the next command.  Remember
					the command that was just processed first in case it is to be
					processed again. */
					strcpy( ( char * ) cLastInputString, ( char * ) cInputString );
					cInputIndex = 0;
					memset( cInputString, 0x00, cmdMAX_INPUT_SIZE );

					/* Ensure the last string to be transmitted has completed. */
					if( FreeRTOS_ioctl( xConsole, ioctlOBTAIN_WRITE_MUTEX, cmd50ms ) == pdPASS )
					{
						/* Start to transmit a line separator, just to make the output
						easier to read. */
						FreeRTOS_write( xConsole, pcEndOfCommandOutputString, strlen( ( char * ) pcEndOfCommandOutputString ) );
					}
				}
				else
				{
					if( UART_rxBuffer == '\r' )
					{
						/* Ignore the character. */
					}
					else if( UART_rxBuffer == '\b' )
					{
						/* Backspace was pressed.  Erase the last character in the
						string - if any. */
						if( cInputIndex > 0 )
						{
							cInputIndex--;
							cInputString[ cInputIndex ] = '\0';
						}
					}
					else
					{
						/* A character was entered.  Add it to the string
						entered so far.  When a \n is entered the complete
						string will be passed to the command interpreter. */
						if( ( UART_rxBuffer >= ' ' ) && ( UART_rxBuffer <= '~' ) )
						{
							if( cInputIndex < cmdMAX_INPUT_SIZE )
							{
								cInputString[ cInputIndex ] = UART_rxBuffer;
								cInputIndex++;
							}
						}
					}
				}
			}
}

#endif
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
  xprintf_init();
  register_commands_cli();
#ifdef TEST
  char *msg = "Till now all fine! you can continue my bike :v";
  xTaskCreate(vLed, "LED1", configMINIMAL_STACK_SIZE, msg, (tskIDLE_PRIORITY+1), NULL);
#else
 xTaskCreate(xCLI, "CLI", (2*configMINIMAL_STACK_SIZE), NULL, (tskIDLE_PRIORITY+1), &xCMDS_UART);
#endif
  vTaskStartScheduler();
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_LDO_SUPPLY);

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV1;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	BaseType_t xHigherPriorityTaskWoken;
	  HAL_UART_Receive_IT(&XPRINTF_UART, &UART_rxBuffer, 1);
	/* xHigherPriorityTaskWoken must be initialised to pdFALSE.  If calling
	    vTaskNotifyGiveFromISR() unblocks the handling task, and the priority of
	    the handling task is higher than the priority of the currently running task,
	    then xHigherPriorityTaskWoken will automatically get set to pdTRUE. */
	    xHigherPriorityTaskWoken = pdFALSE;

	    /* Unblock the handling task so the task can perform any processing necessitated
	    by the interrupt.  xHandlingTask is the task's handle, which was obtained
	    when the task was created. */
	    vTaskNotifyGiveFromISR( xCMDS_UART, &xHigherPriorityTaskWoken );

	    /* Force a context switch if xHigherPriorityTaskWoken is now set to pdTRUE.
	    The macro used to do this is dependent on the port and may be called
	    portEND_SWITCHING_ISR. */
	    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );

}
/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM13 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM13) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
