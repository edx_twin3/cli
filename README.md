CLI FreeRTOS plus for STM32

## What is:
This project implements the FreeRTOS-IO for FreeRTOS-Plus-CLI, this is a port for stm32 mcu from the offical example.
&nbsp;
[More info](https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_CLI/FreeRTOS_Plus_Command_Line_Interface.html)
&nbsp;
As always any doubt, question or comment, i'll try to respond ASAP, you can reach me at:&nbsp;
[edx-Twin3](https://edx-twin3.org)
[edx@edx-twin3.org](mailto://edx@edx-twin3.org)
See ya :1