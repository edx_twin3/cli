/*
 * FreeRTOS_H753_UART.h
 *
 *  Created on: 14 jul 2023
 *      Author: edx
 */

#ifndef FREERTOS_H753_UART_H_
#define FREERTOS_H753_UART_H_

#include "FreeRTOS.h"
#include "FreeRTOS_DriverInterface.h"
#include "usart.h"

#define boardNUM_UARTS		(4) 	//Number of physical UART of the uC


size_t FreeRTOS_UART_write(Peripheral_Descriptor_t const pxPeripheral, const void *pvBuffer, const size_t xBytes);
size_t FreeRTOS_UART_read(Peripheral_Descriptor_t const pxPeripheral, void * const pvBuffer, const size_t xBytes);
portBASE_TYPE FreeRTOS_UART_ioctl(Peripheral_Descriptor_t pxPeripheral, uint32_t ulRequest, void *pvValue);
#endif /* FREERTOS_H753_UART_H_ */
