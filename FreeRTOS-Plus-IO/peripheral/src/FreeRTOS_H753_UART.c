/*
 * FreeRTOS_H753_UART.c
 *
 *  Created on: 14 jul 2023
 *      Author: edx
 */
/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* IO library includes. */
#include "FreeRTOS_IO.h"
#include "IOUtils_Common.h"
#include "FreeRTOS_H753_UART.h"

extern uint8_t UART_rxBuffer;


/* Stores the transfer control structures that are currently in use by the
supported UART ports. */
static Transfer_Control_t *pxTxTransferControlStructs[ boardNUM_UARTS ] = { NULL };
static Transfer_Control_t *pxRxTransferControlStructs[ boardNUM_UARTS ] = { NULL };
/* Stores the IRQ numbers of the supported UART ports. */
static const IRQn_Type xIRQ[] = { USART1_IRQn, USART2_IRQn, USART3_IRQn, UART4_IRQn, UART5_IRQn, USART6_IRQn };


static inline size_t prvFillFifoFromBuffer( UART_HandleTypeDef * const pxUART, uint8_t **ppucBuffer, const size_t xTotalBytes )
{
size_t xBytesSent = xTotalBytes;

	/* This function is only used by zero copy transmissions, so mutual
	exclusion is already taken care of by the fact that a task must first
	obtain a semaphore before initiating a zero copy transfer.  The semaphore
	is part of the zero copy structure, not part of the application. */
//	while( ( pxUART->FIFOLVL & uartTX_FIFO_LEVEL_MASK ) != uartTX_FIFO_LEVEL_MASK )
//	{
//		if( xBytesSent >= xTotalBytes )
//		{
//			break;
//		}
//		else
//		{
//			pxUART->THR = **ppucBuffer;
//			( *ppucBuffer )++;
//			xBytesSent++;
//		}
//	}

xprintf("%s\n", *ppucBuffer);
return xBytesSent;
}

extern portBASE_TYPE FreeRTOS_UART_open(Peripheral_Control_t* const pxPeripheralControl){
	portBASE_TYPE status = pdFAIL;
	const uint8_t cPeripheralNumber = diGET_PERIPHERAL_NUMBER( pxPeripheralControl );
	/* Sanity check the peripheral number. */
		if( cPeripheralNumber < boardNUM_UARTS )
		{
			pxPeripheralControl->read = FreeRTOS_UART_read;
			pxPeripheralControl->write = FreeRTOS_UART_write;
			pxPeripheralControl->ioctl = FreeRTOS_UART_ioctl;
			status = pdPASS;
		}
	return status;
}

extern size_t FreeRTOS_UART_write(Peripheral_Descriptor_t const pxPeripheral, const void *pvBuffer, const size_t xBytes){
	size_t status = pdFAIL;
	Peripheral_Control_t * const pxPeripheralControl = ( Peripheral_Control_t * const ) pxPeripheral;
	UART_HandleTypeDef * const pxUART = ( UART_HandleTypeDef * const ) diGET_PERIPHERAL_BASE_ADDRESS( ( ( Peripheral_Control_t * const ) pxPeripheral ) );
	int8_t cPeripheralNumber;

	if( diGET_TX_TRANSFER_STRUCT( pxPeripheralControl ) == NULL )
	{
		#if ioconfigUSE_UART_POLLED_TX == 1
		{
			/* No FreeRTOS objects exist to allow transmission without blocking
			the	task, so just send out by polling.  No semaphore or queue is
			used here, so the application must ensure only one task attempts to
			make a polling write at a time. */
			status = UART_Send( pxUART, ( uint8_t * ) pvBuffer, ( size_t ) xBytes, BLOCKING );

			/* The UART is set to polling mode, so may as well poll the busy bit
			too.  Change to interrupt driven mode to avoid wasting CPU time here. */
			while( UART_CheckBusy( pxUART ) != RESET );
		}
		#endif /* ioconfigUSE_UART_POLLED_TX */
	}
	else
	{
		/* Remember which transfer control structure is being used.
		The Tx interrupt will use this to continue to write data to the
		Tx FIFO/UART until the length member of the structure reaches
		zero. */
		cPeripheralNumber = diGET_PERIPHERAL_NUMBER( pxPeripheralControl );
		pxTxTransferControlStructs[ cPeripheralNumber  ] = diGET_TX_TRANSFER_STRUCT( pxPeripheralControl );

		switch( diGET_TX_TRANSFER_TYPE( pxPeripheralControl ) )
		{
			case ioctlUSE_ZERO_COPY_TX :

				#if ioconfigUSE_UART_ZERO_COPY_TX == 1
				{
					/* The implementation of the zero copy write uses a semaphore
					to indicate whether a write is complete (and so the buffer
					being written free again) or not.  The semantics of using a
					zero copy write dictate that a zero copy write can only be
					attempted by a task, once the semaphore has been successfully
					obtained by that task.  This ensure that only one task can
					perform a zero copy write at any one time.  Ensure the semaphore
					is not currently available, if this function has been called
					without it being obtained first then it is an error. */
					configASSERT( xIOUtilsGetZeroCopyWriteMutex( pxPeripheralControl, ioctlOBTAIN_WRITE_MUTEX, 0U ) == 0 );
					status = xBytes;
					ioutilsINITIATE_ZERO_COPY_TX
						(
							pxPeripheralControl,
							CLEAR_BIT(pxUART->Instance->CR1, 0) ,/* Disable peripheral function. */
							SET_BIT(pxUART->Instance->CR1, 0),  /* Enable peripheral function. */
							prvFillFifoFromBuffer( pxUART, ( uint8_t ** ) &( pvBuffer ), xBytes ), /* Write to peripheral function. */
							pvBuffer, 						/* Data source. */
							status							/* Number of bytes to be written. This will get set to zero if the write mutex is not held. */
						);
				}
				#endif /* ioconfigUSE_UART_ZERO_COPY_TX */
				break;


			case ioctlUSE_CHARACTER_QUEUE_TX :

				#if ioconfigUSE_UART_TX_CHAR_QUEUE == 1
				{
					/* The queue allows multiple tasks to attempt to write
					bytes, but ensures only the highest priority of these tasks
					will actually succeed.  If two tasks of equal priority
					attempt to write simultaneously, then the application must
					ensure mutual exclusion, as time slicing could result in
					the strings being sent to the queue being interleaved. */
					ioutilsBLOCKING_SEND_CHARS_TO_TX_QUEUE
						(
							pxPeripheralControl,
							( pxUART->LSR & uartTX_BUSY_MASK ) == uartTX_BUSY_MASK,  /* Peripheral busy condition. */
							pxUART->THR = ucChar,				/* Peripheral write function. */
							( ( uint8_t * ) pvBuffer ),			/* Data source. */
							xBytes, 							/* Number of bytes to be written. */
							xReturn );
				}
				#endif /* ioconfigUSE_UART_TX_CHAR_QUEUE */
				break;


			default :
				/* Other methods can be implemented here.  For now set the
				stored transfer structure back to NULL as nothing is being
				sent. */
				configASSERT( status );
				pxTxTransferControlStructs[ cPeripheralNumber ] = NULL;
				/* Prevent compiler warnings when the configuration is set such
				that the following parameters are not used. */
				( void ) pvBuffer;
				( void ) xBytes;
				( void ) pxUART;
				break;
		}
	}


	return status;
}
size_t FreeRTOS_UART_read(Peripheral_Descriptor_t const pxPeripheral, void * const pvBuffer, const size_t xBytes){
	Peripheral_Control_t * const pxPeripheralControl = ( Peripheral_Control_t * const ) pxPeripheral;
	size_t status = 0U;
	if( diGET_RX_TRANSFER_STRUCT( pxPeripheralControl ) == NULL )
		{
			#if ioconfigUSE_UART_POLLED_RX == 1
			{
				/* No FreeRTOS objects exist to allow reception without blocking
				the task, so just receive by polling.  No semaphore or queue is
				used here, so the application must ensure only one task attempts
				to make a polling read at a time. */
				xReturn = UART_Receive( pxUART, pvBuffer, xBytes, NONE_BLOCKING );
			}
			#endif /* ioconfigUSE_UART_POLLED_RX */
		}
		else
		{
			/* Sanity check the array index. */
			configASSERT( diGET_PERIPHERAL_NUMBER( pxPeripheralControl ) < ( int8_t ) ( sizeof( xIRQ ) / sizeof( IRQn_Type ) ) );

			switch( diGET_RX_TRANSFER_TYPE( pxPeripheralControl ) )
			{
				case ioctlUSE_CIRCULAR_BUFFER_RX :

					#if ioconfigUSE_UART_CIRCULAR_BUFFER_RX == 1
					{
						/* There is nothing to prevent multiple tasks attempting to
						read the circular buffer at any one time.  The implementation
						of the circular buffer uses a semaphore to indicate when new
						data is available, and the semaphore will ensure that only the
						highest priority task that is attempting a read will actually
						receive bytes. */
						ioutilsRECEIVE_CHARS_FROM_CIRCULAR_BUFFER
							(
								pxPeripheralControl,
								UART_IntConfig( pxUART, UART_INTCFG_RBR, DISABLE ),	/* Disable peripheral. */
								UART_IntConfig( pxUART, UART_INTCFG_RBR, ENABLE ), 	/* Enable peripheral. */
								( ( uint8_t * ) pvBuffer ),							/* Data destination. */
								xBytes,												/* Bytes to read. */
								status												/* Number of bytes read. */
							);
					}
					#endif /* ioconfigUSE_UART_CIRCULAR_BUFFER_RX */
					break;


				case ioctlUSE_CHARACTER_QUEUE_RX :

					#if ioconfigUSE_UART_RX_CHAR_QUEUE == 1
					{
						/* The queue allows multiple tasks to attempt to read
						bytes, but ensures only the highest priority of these
						tasks will actually receive bytes.  If two tasks of equal
						priority attempt to read simultaneously, then the
						application must ensure mutual exclusion, as time slicing
						could result in the string being received being partially
						received by each task. */
						status = xIOUtilsReceiveCharsFromRxQueue( pxPeripheralControl, ( uint8_t * ) pvBuffer, xBytes );
					}
					#endif /* ioconfigUSE_UART_RX_CHAR_QUEUE */
					break;


				default :

					/* Other methods can be implemented here. */
					configASSERT(status);

					/* Prevent compiler warnings when the configuration is set such
					that the following parameters are not used. */
					( void ) pvBuffer;
					( void ) xBytes;
					break;
			}
		}

		return status;
}

portBASE_TYPE FreeRTOS_UART_ioctl(Peripheral_Descriptor_t pxPeripheral, uint32_t ulRequest, void *pvValue){
	Peripheral_Control_t * const pxPeripheralControl = ( Peripheral_Control_t * const ) pxPeripheral;
	uint32_t ulValue = ( uint32_t ) pvValue;
	const int8_t cPeripheralNumber = diGET_PERIPHERAL_NUMBER( ( ( Peripheral_Control_t * const ) pxPeripheral ) );
	UART_HandleTypeDef * huart = ( UART_HandleTypeDef * ) diGET_PERIPHERAL_BASE_ADDRESS( ( ( Peripheral_Control_t * const ) pxPeripheral ) );
	portBASE_TYPE xReturn = pdPASS;

		/* Sanity check the array index. */
		configASSERT( cPeripheralNumber < ( int8_t ) ( sizeof( xIRQ ) / sizeof( IRQn_Type ) ) );

		taskENTER_CRITICAL();
		{
			switch( ulRequest )
			{
				case ioctlUSE_INTERRUPTS :

					if( ulValue == pdFALSE )
					{
						NVIC_DisableIRQ( xIRQ[ cPeripheralNumber ] );
					}
					else
					{
						/* Enable the Rx and Tx interrupt. */
//XXX
//						HAL_UART_Transmit_IT(XPRINTF_UART, &UART_rxBuffer, 1);
						HAL_UART_Receive_IT(&XPRINTF_UART, &UART_rxBuffer, 1);

						/* Enable the interrupt and set its priority to the minimum
						interrupt priority.  A separate command can be issued to raise
						the priority if desired.
						NOTE: STM32 peripheral starts from 1 rather than 0 that's why the minus 1*/
						__NVIC_SetPriority( xIRQ[ cPeripheralNumber - 1], configLIBRARY_LOWEST_INTERRUPT_PRIORITY );
						NVIC_EnableIRQ( xIRQ[ cPeripheralNumber -1 ] );

						/* If the Rx is configured to use interrupts, remember the
						transfer control structure that should be used.  A reference
						to the Tx transfer control structure is taken when a write()
						operation is actually performed. */
						pxRxTransferControlStructs[ cPeripheralNumber ] = pxPeripheralControl->pxRxControl;
					}
					break;


				case ioctlSET_SPEED :

					/* Set up the default UART configuration. */
					  CLEAR_BIT(huart->Instance->CR1,0);   //Disable UART
					  huart->Init.BaudRate = ulValue;
					  huart->Init.WordLength = UART_WORDLENGTH_8B;
					  huart->Init.StopBits = UART_STOPBITS_1;
					  huart->Init.Parity = UART_PARITY_NONE;
					  huart->Init.Mode = UART_MODE_TX_RX;
					  huart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
					  SET_BIT(huart->Instance->CR1,0); //Enable UART
					break;


				case ioctlSET_INTERRUPT_PRIORITY :

					/* The ISR uses ISR safe FreeRTOS API functions, so the priority
					being set must be lower than (ie numerically larger than)
					configMAX_LIBRARY_INTERRUPT_PRIORITY. */
					//configASSERT( ulValue >= configMAX_LIBRARY_INTERRUPT_PRIORITY );
					NVIC_SetPriority( xIRQ[ cPeripheralNumber - 1], ulValue );
					break;


				default :

					xReturn = pdFAIL;
					break;
			}
		}
		taskEXIT_CRITICAL();

		return xReturn;
}
