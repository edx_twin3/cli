/*
 * commands.h
 *
 *  Created on: 7 ago 2023
 *      Author: edx
 */

#ifndef INC_COMMANDS_H_
#define INC_COMMANDS_H_

#include <stddef.h>
#include <stdint.h>

#include "FreeRTOS.h"
#include "FreeRTOSIOConfig.h"
#include "FreeRTOS_CLI.h"
#include "projdefs.h"
#include "portmacro.h"
#include "edx_xprintf.h"
void register_commands_cli(void);

#endif /* INC_COMMANDS_H_ */
