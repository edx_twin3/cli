/*
 * commands.c
 *
 *  Created on: 7 ago 2023
 *      Author: edx
 */

#include "commands.h"

static portBASE_TYPE prvRunTestCMD(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
	xprintf("Hole munde, este es un comande :v\n");
return 0;
}


static const CLI_Command_Definition_t prvTaskTest =
{
	( const char * const ) "test", /* The command string to type. */
	( const char * const ) "test:\r\n Displays an amusing hello world cli command test\r\n\r\n",
	prvRunTestCMD, /* The function to run. */
	0 /* No parameters are expected. */
};

extern void register_commands_cli(void){
	FreeRTOS_CLIRegisterCommand(&prvTaskTest);
}
